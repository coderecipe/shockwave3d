--Function takes a rigid body object, rigidBody = physx.getrigidbody("name")
on visualize_proxy rigidBody
  p3dmember = member("my3dWorld")
  physX = member("physX")
  if rigidBody <> void then
    --rb.properties returns a list containing: numFaces,numVertices,vertexList,face
    proxy_list = rigidBody.properties
    --Note that the mesh details are vailable only for rigidbody of shape
    --#convex and #concave
    if rigidBody.shape = #convex or rigidBody.shape = #concave then
      numFaces = proxy_list.numFaces
      numVertices = proxy_list.numVertices
      newMesh_res = p3dmember.newMesh(rigidBody.name & "_proxy_res",numFaces ,numVertices, 0, 3)
      --the mesh color would be gray, change the values if needed
      newMesh_res.colorList = [rgb(150,150,150),rgb(150, 150, 150),rgb(150,150,150)]
      vertex_list = proxy_list.vertexList
      repeat with i=1 to numVertices then
        newMesh_res.vertexList[i]  = vector(vertex_list[i][1],vertex_list[i][2],vertex_list[i][3])
      end repeat
      repeat with f=1 to numFaces then
        newMesh_res.face[f].vertices = proxy_list.face[f].vertices
      end repeat
      newMesh_res.generateNormals(#smooth)
 
      --build the mesh using the above defined mesh resource
      newMesh_res.build()
      nm = p3dmember.newModel(rigidBody.name & "_visualize", newMesh_res)
      nm.transform.position = rigidBody.position
    else
      if rigidBody.shape = #box then
        newMesh_res = p3Dmember.newModelResource(rigidBody.name & "_proxy_res",#box)
        newMesh_res.width = proxy_list.width
        newMesh_res.height = proxy_list.height
        newMesh_res.length = proxy_list.length
      else
        --would be #sphere shape
        newMesh_res = p3Dmember.newModelResource(rigidBody.name & "_proxy_res",#sphere)
        newMesh_res.radius = proxy_list.radius
        newMesh_res.resolution = 10
      end if
      nm = p3dmember.newModel(rigidBody.name & "_visualize", newMesh_res)
      --.center is a new property , helps make sure that the
      --centers of the model and visualize_proxy match
      nm.transform.position = rigidBody.position + proxy_list.center
    end if
    --Add the newly created mesh as a child to the 3d model.
    p3dmember.model(rigidBody.name).addChild(p3dmember.model(rigidBody.name & "_visualize"))
    --render the proxy/hull in wireframe
    nm.shader.renderstyle = #wire
  end if
end

